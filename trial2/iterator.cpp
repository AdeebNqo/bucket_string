#include "iterator.hpp"
#include<iostream>
namespace mhlzol004{
	iterator::iterator(linked_list& cont){
		pos =0;		
		container = &cont;
		curr_pointer = &((*container)[pos]);
	}
	iterator::iterator(const iterator& original){
		pos = original.pos;
		container = original.container;
		//container = new linked_list(*(original.container));
		curr_pointer = &((*container)[pos]);
	}
	iterator& iterator::operator=(const iterator& original){
		pos = original.pos;
		container = original.container;
		curr_pointer = &((*container)[pos]);
		return *this;
	}
	iterator::~iterator(){
		//there's nothing to delete
	}
	char& iterator::operator*(){
		curr_pointer = &((*container)[pos]);
		return *curr_pointer;
	}
	void iterator::operator--(){
		--pos;
	}
	void iterator::operator--(int){
		pos--;
	}
	void iterator::operator++(){
		++pos;
	}
	void iterator::operator++(int){
		pos++;
	}
	void iterator::operator-(unsigned int offset){
		pos-=offset;
	}
	void iterator::operator+(unsigned int offset){
		pos += offset;
	}
	void iterator::end(){
		pos  = (*container).length();
	}
	bool iterator::eof(){
		int total = (*container).length();
		if (pos>=total){
			return true;
		}
		return false;
	}
}
