#include "bucket_string.hpp"
#include "iterator.hpp"
#include<iostream>
#include<string>
namespace mhlzol004{
	bucket_string::bucket_string(){
		std::cout<<"Default constructor called!"<<std::endl;
		bucket_size = 7;
		is_copy = false;
		buckets = new linked_list();
	}
	bucket_string::bucket_string(unsigned int mybucket_size){
		bucket_size = mybucket_size;
		is_copy = false;
		buckets = new linked_list(mybucket_size);
	}
	bucket_string::~bucket_string(){
		std::cout<<"Desctructor called!"<<std::endl;
		if (!is_copy){
			delete buckets;
			buckets = NULL;
		}
	}
	bucket_string::bucket_string(bucket_string &original){
		is_copy = false;
		std::cout<<"Copy constructor called!"<<std::endl;
		linked_list *tmp = new linked_list(*original.buckets);
		buckets = tmp;
		bucket_size = original.bucket_size;
	}
	bucket_string& bucket_string::operator=(const bucket_string& original){
		std::cout<<"Assignment operator called!"<<std::endl;
		buckets = original.buckets;
		bucket_size = original.bucket_size;
		is_copy = true;
	}
	char& bucket_string::operator[](int pos){
		return (*buckets)[pos];
	}
	void bucket_string::push_back(std::string some_string){
		buckets->push_back(some_string);
	}
	size_t bucket_string::length(){
		return buckets->length();
	}
	//debug
	void bucket_string::print(){
		buckets->print();	
	}
	//iterator methods and logic
	iterator bucket_string::begin(){
		iterator *tmp = new iterator(*buckets);
		return *tmp;
	}
	iterator bucket_string::end(){
		iterator *tmp = new iterator(*buckets);
		(*tmp).end();
		return *tmp;
	}
	
	// I/O operators

	//writing
	std::ostream& operator<<(std::ostream& output, bucket_string& some_string){
		int string_length = some_string.length();
		char write_string[string_length];
		iterator curr = some_string.begin();	
		while(!curr.eof()){
			write_string[curr.pos] = *curr;
			output<<write_string[curr.pos];
			++curr;
		}
		output<<std::endl;
		return output;
	}
	//reading
	std::istream& operator>>(std::istream& input, bucket_string& some_string){
		std::string tmp;
		input >> tmp;
		some_string.push_back(tmp);
		return input;
	}
	void bucket_string::replace(iterator& first, iterator& last, bucket_string& bs){
		int tmp_first_pos = first.pos;
		int tmp_last_pos = last.pos;
		
		unsigned int range = last.pos-first.pos;
		iterator curr_bs = bs.begin();
		for (int i=0;i<range;i++){
			*first = *curr_bs;
			++first;
			++curr_bs;
		}

		first.pos = tmp_first_pos;
		last.pos = tmp_last_pos;
	}
	bucket_string& bucket_string::substr(iterator& first, iterator& last){
		int tmp_first_pos = first.pos;
		int tmp_last_pos = last.pos;
	
		bucket_string *tmp_string = new bucket_string(bucket_size);
		unsigned int range = last.pos - first.pos;
		for (int i=0;i<range;i++){
			std::string tmp = std::string(1,*first);
			tmp_string->push_back(tmp);
			++first;
		}
		
		first.pos = tmp_first_pos;
		last.pos = tmp_last_pos;
		return  *tmp_string;
	}
	void bucket_string::insert(iterator first, bucket_string bs){
		bucket_string *new_string = new bucket_string(bucket_size);
		iterator front = this->begin();
		//extracting the front end		
		while(front.pos<=first.pos){
			std::string front_tmp(1,*front);
			new_string->push_back(front_tmp);
			++front;
		}
		//extracting the new part of the string
		int bs_length = bs.length();
		iterator it = bs.begin();
		for (int i=0;i<bs_length;i++,++it){
			std::string newfront_tmp(1,*it);
			new_string->push_back(newfront_tmp);
		}
		//filling the remaining part of the string
		int range = bucket_string::length()-front.pos;
		for (int i=0;i<range;i++){
			std::string front_tmp(1,*front);
			new_string->push_back(front_tmp);
			++front;
		}
		this->operator=(*new_string);
	}
}
