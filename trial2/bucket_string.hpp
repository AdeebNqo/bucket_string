#ifndef _bucket
#define _bucket
#include "linked_list.hpp"
#include<string>
#include<stddef.h>
#include "iterator.hpp"
namespace mhlzol004{
	class iterator; //foward declaration

	class bucket_string{
		public:
			bucket_string();
			bucket_string(unsigned int bucket_size);
			bucket_string(bucket_string &original);
			~bucket_string();
			bucket_string& operator=(const bucket_string& original);
			char& operator[](int pos);
			void push_back(std::string some_string);
			size_t length();
			//debug
			void print();
			//iterator methods
			iterator begin();
			iterator end();

			// I/O operators
			friend std::ostream& operator<<(std::ostream& output, bucket_string& some_string);//writing
			friend std::istream& operator>>(std::istream& input, bucket_string& some_string);//reading

			void replace(iterator& first, iterator& last, bucket_string& bs);
			bucket_string& substr(iterator& first, iterator& last);
			void insert(iterator first, bucket_string bs);
			bool is_copy;
		private:
			linked_list *buckets;
			unsigned int bucket_size;
	};
}
#endif
