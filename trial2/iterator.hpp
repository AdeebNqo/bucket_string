#ifndef _it
#define _it
#include "linked_list.hpp"
#include "bucket_string.hpp"
namespace mhlzol004{
	class bucket_string;

	class iterator{
		public:	
			friend class mhlzol004::bucket_string;
			linked_list* container;
			unsigned int pos;
			char* curr_pointer;
			char& operator*();
			void operator--();
			void operator--(int);
			void operator++();
			void operator++(int);
			void operator-(unsigned int offset);
			void operator+(unsigned int offset);
			void end();
			bool eof();
			~iterator();
			iterator(const iterator &original);
		private:
			iterator(){pos=0;container=NULL;curr_pointer=NULL;};
			iterator(linked_list& container);
			iterator& operator=(const iterator& original);
	};
}
#endif
