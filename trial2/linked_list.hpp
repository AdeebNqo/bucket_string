#ifndef _list
#define _list

#include<string>
#include "node.hpp"
#include<stddef.h>
namespace mhlzol004{
	class linked_list{
		public:
			linked_list();
			linked_list(unsigned int bucket_size);
			linked_list(const linked_list& original);
			~linked_list();
			char& operator[](unsigned int pos);
			linked_list& operator=(const linked_list& original);			

			node* get_head() const;
			node* get_tail() const;

			void push_back(std::string new_string);
			void push_back(char new_string[]);
			void push_back(const char *new_string, unsigned int size);
	
			void print();
			size_t length();
			unsigned int get_bucket_size() const;
	
		private:
			node *head;
			node *tail; //for reducing insertion time
			unsigned int bucket_size;

			void copy_node(node* from, node* to);
	};
}
#endif
