#ifndef _Node
#define _Node
#include<stddef.h>
namespace mhlzol004{
	class node{
		public:
			node();
			node(unsigned int bucket_size);
			~node();
			node(node &original);
			node& operator=(node& original);

			//gets the following node
			node* get_next();
			//sets the following node
			void set_next(node *next_node);
			//stores a given array of chars in this node
			void store_data(char chars[]);
			//returns the stored char array
			char* get_data();
			//get the size of each bucket
			unsigned int get_bucket_size();
	
			//method for printing contents of a bucket/node
			void print();
			/*

			The following variables are for taking into account the fact that a bucket may not be full
			
			*/
			bool is_full;
			unsigned int empty_slots;
			//this method is to be used only to fill the
			//node when there are remaning slots
			void fill_array(char *some_array);
			void clear_data();
			size_t length();
			void set_length(unsigned int new_len);
		private:
			unsigned int len;
			char *x;
			node* next; //next node
	};
}
#endif
