
bucket string
==============

Info
------
Assignment 2, CSC3022H , Zola Mahlaza <adeebnqo@gmail.com>


Compilation
-------------
Here are the rules required for compilation and running

Compiling:

		make

Running:

		make run

Cleaning up binary files:

		make clean


Contents of tarball
--------------------

1. `Makefile`
2. `bucket_string.cpp` (`bucket_string.hpp`)
3. `cmdline_parser.cpp` (`cmdline_parser.hpp`)
4. `driver.cpp`
5. `iterator.cpp` (`iterator.hpp`)
6. `linked_list.cpp` (`linked_list.hpp`)
7. `node.cpp` (`node.hpp`)

The contents of all files is somewhat evident from the title of each file.

I have included dummy print statement in my constructors. e.g

		bucket_string::~bucket_string(){
			std::cout<<"Desctructor called!"<<std::endl;
			...
		}

The aim is to show you that they really do work.


Output
-------------

The driver class contains the testing of the entire class according to section: 3. Testing
of the assignment specifications.

Test of each section has a header. e.g

		_____________________________________
		operator<< and operator>>
		_____________________________________

		Default constructor called!
		reading from file...
		...


Additional
------------

If you do not trust my test cases, feel free to modifiy them.
(you might need to read through the code in the driver class to be familiar with my bucket_string) ;-)

I also used the cmdline_parser for 'retrieving' filename. I only use the file when illustrating `operator<<` and `operator>>`.
You can change everything if you want in the driver class.
