#include "node.hpp"
#include<iostream>
mhlzol004::node::node(){
	len = 7;
	x = new char[len];
	is_full = false;
	empty_slots = len;

	mhlzol004::node::clear_data();
	next = NULL;
}
mhlzol004::node::node(unsigned int bucket_size){
	len = bucket_size;
	x = new char[len];
	next = NULL;
	is_full = false;
	empty_slots = len;

	mhlzol004::node::clear_data();
}
mhlzol004::node::node(node& original){
	len = original.length();
	empty_slots = original.empty_slots;
	is_full = original.is_full;
	next = original.next;
	x = new char[len];
	for (int i=0;i<len;i++){
		x[i] = original.get_data()[i];
	}
}
mhlzol004::node::~node(){
	delete [] x;
	x = NULL;
	delete next;
	next = NULL;
}
mhlzol004::node& mhlzol004::node::operator=(mhlzol004::node& original){
	std::cout<<"(n) assignment operator!"<<std::endl;
	is_full = original.is_full;
	empty_slots = original.empty_slots;
	len = original.get_bucket_size();
	next =  original.get_next();
	x = original.get_data();
	return *this;
}
//returns the next node
mhlzol004::node* mhlzol004::node::get_next(){
	return next;
}
//sets the next node
void mhlzol004::node::set_next(node *next_node){
	next = next_node;
}
//stores a char array in the current node
void mhlzol004::node::store_data(char chars[]){
	for (int i=0;i<len;i++){
		x[i] = chars[i];
	}
}
//returns the stored array of chars
char* mhlzol004::node::get_data(){
	return x;
}
//returns the size of an individual bucket
unsigned int mhlzol004::node::get_bucket_size(){
	return len;
}
void mhlzol004::node::print(){
	for (int i=0;i<len;i++){
		std::cout<<"["<<x[i]<<"]";
	}
	std::cout<<"\n";
}
void mhlzol004::node::fill_array(char *some_array){
	int start = len-empty_slots;
	for (int i=0;i<empty_slots;i++,start++){
		x[start] = some_array[i];
	}
}
void mhlzol004::node::clear_data(){
	for (int i=0;i<len;i++){
		x[i] = '\0';
	}
}
size_t mhlzol004::node::length(){
	if (next!=NULL){
		return len;
	}
	return len-empty_slots;
}
void mhlzol004::node::set_length(unsigned int length){
	len = length;
}
