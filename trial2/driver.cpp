#include "bucket_string.hpp"
#include<string>
#include<iostream>
#include "cmdline_parser.h"
#include<fstream>
int main(int argc, char * argv[]){
	using namespace mhlzol004;
	//filename
	std::string filename;
	// Instantiate the parser
	cmdline_parser parser;

	// Try parse command line arguments
	if(!parser.process_cmdline(argc, argv))
	{
		// Complain to the standard error stream
		std::cerr << "Couldn't process command line arguments" << std::endl;
		return 1;
	}
	filename = parser.get_database_filename();

	
	std::cout<<"_____________________________________"<<std::endl;
	std::cout<<"Four Special Member Functions"<<std::endl;
	std::cout<<"_____________________________________\n"<<std::endl;
	std::string some_string("Hello World");
	std::string some_string2("Goodbye World");
	/*
	Default constructor &
	deconstructor
	*/
	bucket_string *my_first_string = new bucket_string;
	delete my_first_string;
	
	std::cout<<"-------------------------------------"<<std::endl;
	/*
	Default & Copy constructor
	*/
	bucket_string my_second_string;
	my_second_string.push_back(some_string2);
	my_second_string.push_back(some_string);
	bucket_string my_third_string = my_second_string;
	
	std::cout<<"-------------------------------------\n"<<std::endl;

	/*
	Default constructor &
	Assignment operator
	*/
	bucket_string original;
	original.push_back(some_string);
	bucket_string fake;
	fake = original;
	fake.push_back(some_string2);


	std::cout<<"_____________________________________"<<std::endl;
	std::cout<<"operator<< and operator>>"<<std::endl;
	std::cout<<"_____________________________________\n"<<std::endl;
	bucket_string file_bucket;
	std::fstream my_file;
	my_file.open(filename.c_str());

	std::cout<<"reading from file..."<<std::endl;
	my_file >> file_bucket;
	std::cout<<"bucket string looks like: "<<std::endl;
	file_bucket.print();
	std::cout<<"closing file..."<<std::endl;
	my_file.close();

		
	std::cout<<"\nwriting to file..."<<std::endl;
	std::fstream my_other_file;
	my_other_file.open(filename.c_str());
	my_other_file<<file_bucket;
	std::cout<<"closing file..."<<std::endl;
	my_file.close();
	std::cout<<"-------------------------------------\n"<<std::endl;

	std::cout<<"_____________________________________"<<std::endl;
	std::cout<<"replace and substr."<<std::endl;
	std::cout<<"_____________________________________\n"<<std::endl;
	
	std::string test_string("testing substring and replace");
	bucket_string bs;
	bs.push_back(test_string);

	std::cout<<"1.\tsubstring\n"<<std::endl;
	
	iterator b = bs.begin();
	iterator e = bs.end();
	e-10; 

	std::cout<<"Test case 1: The substring is\n"<<std::endl;

	std::cout<<"bucket string before substr() call:\n"<<std::endl;
	bs.print();
	std::cout<<"bucket string after substr() call:\n"<<std::endl;
	bucket_string case1 = bs.substr(b,e);
	case1.print();

	std::cout<<"Test case 2: The substring is\n"<<std::endl;
	b+3;
	e-3;
	std::cout<<"bucket string before substr() call:\n"<<std::endl;
	bs.print();
	std::cout<<"bucket string after substr() call:\n"<<std::endl;
	bucket_string case2 = bs.substr(b,e);
	case2.print();
	
	std::cout<<"-------------------------------------\n"<<std::endl;
	
	std::cout<<"2.\treplace"<<std::endl;
	
	std::string test_string_replace("abcefghif");
	bucket_string replace_bs;
	replace_bs.push_back(test_string_replace);
	std::cout<<"Test case 1: The new bucket_string is\n"<<std::endl;
	
	iterator replace_b = bs.begin();
	iterator replace_c = bs.begin();
	replace_c+9;
	
	std::cout<<"bucket string before replacement:\n"<<std::endl;
	bs.print();
	std::cout<<"bucket string after replacement:\n"<<std::endl;
	bs.replace(replace_b,replace_c,replace_bs);
	bs.print();

	std::cout<<"-----------------THE END----------------\n"<<std::endl;
	return 0;
}
