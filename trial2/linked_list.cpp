#include "linked_list.hpp"
#include<math.h>
#include<iostream>
mhlzol004::linked_list::linked_list(){
	bucket_size = 7;
	head = NULL;
	tail = NULL;
}
mhlzol004::linked_list::linked_list(unsigned int the_bucket_size){
	this->bucket_size = the_bucket_size;
	head = NULL;
	tail = NULL;
}
mhlzol004::linked_list::linked_list(const mhlzol004::linked_list& original){
	/*
	Variables to be copied from the nodes
	
	len;
	x;
	next;
	empty_slots;
	is_full	

	*/

	bucket_size = original.bucket_size;
	node *new_head = new node();//making new head
	head = new_head;
	node *originals = original.head;
	mhlzol004::linked_list::copy_node(originals, new_head);//copying original head to new head
	
	int bit_switch =0;//damage control. Was havin' issues when deleting copied object. It
			  //supposed to be NULL but the copy made an object which leads to other complications

	if (originals!=NULL){

		bit_switch =1;

		node* prev_node = new_head;
		node* original_curr_node = originals->get_next();
		node* new_curr_node;
		while(original_curr_node!=NULL){
			//copying original current node to new node
			new_curr_node = new node();
			mhlzol004::linked_list::copy_node(original_curr_node, new_curr_node);
			//connecting previous node to new current node
			prev_node->set_next(new_curr_node);
			prev_node = new_curr_node;
			original_curr_node = original_curr_node->get_next();
		}
		prev_node->set_next(NULL);
	}
	if (bit_switch==0){
		head = NULL;
	}
}
mhlzol004::linked_list::~linked_list(){
	node *curr_node;
	node *next = head;
	do{
		if (next!=NULL){
			curr_node = next;
			next = next->get_next();
			delete curr_node;
		}
		else{
			break;
		}
		
	}while(curr_node!=NULL);
	head = NULL;
}
char& mhlzol004::linked_list::operator[](unsigned int pos){
	int node_num = ceil((pos+1)/(1.0*bucket_size));
	int relative_pos = pos%bucket_size;
	//searching for the node_num'th node
	node* curr_node = head;
	for (int i=1;i<node_num;i++){
		curr_node = curr_node->get_next();
	}
	char *return_val = &(curr_node->get_data())[relative_pos];
	return *return_val;
}
mhlzol004::linked_list& mhlzol004::linked_list::operator=(const linked_list& original){
	std::cout<<"(ll) assignement operator called"<<std::endl;
	head = original.get_head();
	tail = original.get_tail();
	bucket_size = original.get_bucket_size();
}
void mhlzol004::linked_list::copy_node(node* from, node* to){
	if (from!=NULL){
		int bs = from->get_bucket_size();
		to->set_length(bs);
		to->empty_slots = from->empty_slots;
		to->is_full = from->is_full;
		for (int i=0;i<bucket_size;i++){
			*(to->get_data()+i) = *(from->get_data()+i);
		}
	}
}
mhlzol004::node* mhlzol004::linked_list::get_head() const{
	return head;
}
mhlzol004::node* mhlzol004::linked_list::get_tail() const{
	return tail;
}  
unsigned int mhlzol004::linked_list::get_bucket_size() const{
	return bucket_size;
}
//inserting a string object into the linked_list
void mhlzol004::linked_list::push_back(std::string new_string){
	unsigned int size = new_string.size();
	mhlzol004::linked_list::push_back(new_string.c_str(),size);
}
void mhlzol004::linked_list::push_back(const char *new_string, unsigned int size){
	bool more = true;

	if (head==NULL){
		head = new node();
		tail = head;
	}
	int new_string_pos =0;
	int empty_slots= tail->empty_slots;
	if (!(tail->is_full)){
		if (size<=empty_slots){
			char *filler = new char[empty_slots];
			for (int i=0;i<size;i++){
				filler[i] = new_string[i];
			}
			for (int i=size;i<empty_slots;i++){
				filler[i] = '\0';
			}
			tail->fill_array(filler);
			tail->empty_slots-=size;
			if (tail->empty_slots==0){
				tail->is_full = true;
			}
			more = false;
		}
		else{
			char *filler = new char[empty_slots];
			for (int i=0;i<empty_slots;i++,new_string_pos++){
				filler[i] = new_string[new_string_pos];
			}
			tail->fill_array(filler);
			tail->is_full = true;
			tail->empty_slots =0;
			if (size==empty_slots){
				more = false;
			}
		}
	}

	if (more){
		int num_buckets = ceil((size-empty_slots)/((1.0)*bucket_size));
		int rem = ((size-empty_slots)%bucket_size);
		for (int i=0;i<num_buckets;i++){
			char *data = new char[bucket_size];
			//initialising array
			for (int j=0;j<bucket_size;j++){
				data[j] = '\0';
			}
			//spliting char's to nodes

			for (int j=0;!(j!=0 && (j%bucket_size ==0));j++,new_string_pos++){
				if(new_string_pos>size){
					break;
				}
				data[j] = new_string[new_string_pos];
			}
			node *new_node = new node();
			new_node->fill_array(data);
			if (i!=num_buckets-1){
				new_node->is_full = true;
				new_node->empty_slots = 0;
			}
			else{
				new_node->is_full = false;
				new_node->empty_slots = bucket_size-rem;
			}
			tail->set_next(new_node);
			tail = new_node;
		}
	}
}
/*
void mhlzol004::linked_list::push_back(const char *new_string, unsigned int size){
	//getting the number of buckets we have to create
	int num_buckets = ceil(size/((1.0)*bucket_size));
	//creating buckets
	int curr_pos =0;
	for (int i=1;i<=num_buckets;i++){
		int j;
		char x[bucket_size];
		/*
		Initialise array to have NULL's everywhere
		
		for (int pos=0;pos<bucket_size;pos++){
			x[pos] = '\0';
		}
		//var for keeping the number of characters we have inserted into the bucket.
		int num_of_characters =0;
		//creating a array to store in a bucket
		for (curr_pos,j=0;curr_pos<size;curr_pos++,j++){
			if (curr_pos==(i*bucket_size)){
				break;
			}
			x[j] = new_string[curr_pos];
			++num_of_characters;
		}
		//creating the new node/bucket
		node *tmp_node = new node(bucket_size);
		tmp_node->store_data(x);
		if (num_of_characters==bucket_size){
			tmp_node->is_full = true;
		}
		tmp_node->empty_slots = bucket_size-num_of_characters;
		//adding new node as the head if there is no head
		if (head==NULL){
			std::cout<<"head is NULL"<<std::endl;
			head = tmp_node;
			tail = head;
			head->set_next(NULL);
		}
		//when there is a head
		else{
			//f the tail is already filled up
			if (tail->is_full){
				//connecting new node to the tail
				tail->set_next(tmp_node);
				//making new node the tail
				tail = tmp_node;
			}
			else{
				/*
				
				When the tail node is not empty				

				
				//get the num of remaining slots from the bucket
				int remaining_slots = tail->empty_slots;
				//if the new node has less characters
				// than the missing characters in existing tail
				if (num_of_characters<remaining_slots){
					std::cout<<"num_of_characters<remaining_slots"<<std::endl;
					char tmp_remaining_chars[remaining_slots];
					//initialising array
					for (int tmp_pos=0;tmp_pos<remaining_slots;tmp_pos++){
						tmp_remaining_chars[tmp_pos] = '\0';
					}
					for (int tmp_pos=0;tmp_pos<num_of_characters;tmp_pos++){
						tmp_remaining_chars[tmp_pos] = x[tmp_pos];
					}
					tail->fill_array(tmp_remaining_chars);
					tail->empty_slots -= num_of_characters;
				}
				else{
				
					copying characters from new node to existing tail and
					moving them to existing tail
					
					std::cout<<"++++++++++++++++++++++++++++++++"<<std::endl;
					std::cout<<"current node: "<<std::endl;
					tmp_node->print();
					std::cout<<"num_of_characters: "<<num_of_characters<<std::endl;
					std::cout<<"remaining slots: "<<remaining_slots<<std::endl;
					std::cout<<"tail is: "<<std::endl;
					tail->print();
					char copy_char[remaining_slots];
					int copy_char_pos;
					for (copy_char_pos=0;copy_char_pos<remaining_slots;copy_char_pos++){
						copy_char[copy_char_pos] = x[copy_char_pos];
					}
					tail->fill_array(copy_char);
					//std::cout<<"tail is: "<<std::endl;
					//tail->print();
					
					Moving the remaining chars to a new node
					if there are any
					
				//	std::cout<<"remaining slots "<<remaining_slots<<std::endl;
				//	std::cout<<"before shift!"<<std::endl;
					for (int a=0;a<bucket_size;a++){
						std::cout<<"["<<x[a]<<"]";
					}
					std::cout<<"\n";

					if (remaining_slots!=num_of_characters){
						for (int left_shift=0;left_shift<bucket_size;left_shift++){
							if (left_shift<bucket_size-remaining_slots){
								x[left_shift] = x[left_shift+remaining_slots];
							}
							else{
								x[left_shift] = '\0';
							}
						}

						tmp_node->clear_data();
						tmp_node->store_data(x);
						tail->set_next(tmp_node);
						tail = tmp_node;
						tail->empty_slots += remaining_slots;
						tail->is_full = false;
					}

					std::cout<<"current node is now: "<<std::endl;
					tail->print();
					std::cout<<"++++++++++++++++++++++++++++++++"<<std::endl;
					std::cout<<"after shift!"<<std::endl;
					for (int a=0;a<bucket_size;a++){
						std::cout<<"["<<x[a]<<"]";
					}
					std::cout<<"\n";
				}
			}
		}
	}
}*/
void mhlzol004::linked_list::print(){
	node * curr_node = head;
	int count = 0;
	while(curr_node!=NULL){
		curr_node->print();
		curr_node = curr_node->get_next();
	}
}
size_t mhlzol004::linked_list::length(){
	size_t length = 0;
	node *curr = head;
	while(curr!=NULL){
		length+= curr->length();
		curr = curr->get_next();
	}
	return length;
}
